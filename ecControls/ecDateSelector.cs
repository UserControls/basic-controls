﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ecControls
{
    /// <summary>
    /// This is a user control utilizing the modified DateTimePicker
    /// It implements the ecText Class and overrides the non-base components
    /// </summary>
    public partial class ecDateSelector : ecText
    {
        #region Input Component
        // Specifies the modified DateTimePicker to be added
        new internal xDateTime InputControl;
        #endregion

        #region Variables
        private string _valueDay;
        private string _valueMonth;
        private string _valueDate;
        private string _valueTime;
        #endregion

        #region Exposed Properties
        public Color FontColor
        {
            get
            {
                return base.ForeColor;
            }
            set
            {
                base.ForeColor = value;
                InputControl.ForeColor=value;
                DrawControl();
            }
        }

        public string ValueDay
        {
            get
            {
                return _valueDay;
            }
        }

        public string ValueMonth
        {
            get
            {
                return _valueMonth;
            }
        }

        public string ValueDate
        {
            get
            {
                return _valueDate;
            }
        }

        public string ValueTime
        {
            get
            {
                return _valueTime;
            }
        }

        #endregion

        #region Hide Unused Base Properties and Events
        [Browsable(false)] // Hides this property in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new HorizontalAlignment InputAlignment
        {
            get
            {
                return HorizontalAlignment.Left;
            }
        }

        [Browsable(false)] // Hides this property in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new bool Mask
        {
            get
            {
                return false;
            }
        }

        [Browsable(false)] // Hides this property in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new int MaxChars
        {
            get
            {
                return 0;
            }
        }

        [Browsable(false)] // Hides this property in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new CharacterCasing ValueCasing
        {
            get
            {
                return CharacterCasing.Normal;
            }
        }

        [Browsable(false)] // Hides this Event in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this Event from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new EventHandler TextHasChanged;
        #endregion


        #region Exposed Event
        // Event raised when the content of the control changes
        public EventHandler SelectionHasChanged;
        #endregion

        #region Methods
        /// <summary>
        /// Method trigerred when control value is set
        /// </summary>
        internal override void ReturnValueSet()
        {
            InputControl.Value = _controlValue;
        }

        /// <summary>
        /// Clears the local variables that stores the controls value that correspond 
        /// to the specific input component of the control
        /// </summary>
        internal override void ClearComponents()
        {
            _controlValue = DateTime.Now;
            InputControl.Value = _controlValue;
            _valueDay = string.Empty;
            _valueMonth = string.Empty;
            _valueDate = string.Empty;
            _valueTime = string.Empty;
        }

        /// <summary>
        /// Method to add or remove event handler specific for the input component of the control
        /// </summary>
        /// <param name="addRemove">Indicates whether to add or remove the event</param>
        internal override void EventHandlerForComponent(ecAction addRemove)
        {
            if (addRemove == ecAction.Add)
            {
                //this.InputControl.SelectedValueChanged += new EventHandler(Value_Changed);
                this.InputControl.ValueChanged += new EventHandler(Value_Changed);
            }
            else
            {
                //this.InputControl.SelectedValueChanged -= new EventHandler(Value_Changed);
                this.InputControl.ValueChanged -= new EventHandler(Value_Changed);
            }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Method to handle Date Selector Value change Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal override void Value_Changed(object sender, EventArgs e)
        {
            _controlValue = InputControl.Value;

            _valueDay = InputControl.Value.ToString("dddd");
            _valueMonth = InputControl.Value.ToString("MMM");
            _valueDate = InputControl.Value.ToString("MM-dd-yyyy");
            _valueTime = InputControl.Value.ToString("hh:mm");

            // Raise the event to the concrete class
            SelectionHasChanged?.Invoke(sender, e);
        }
        #endregion

        #region Initializers
        /// <summary>
        /// Assigns initial values to the local variables specific to the component
        /// </summary>
        internal override void InitializeSpecialVariables()
        {
            _controlValue = DateTime.Now;

            _valueDay = _controlValue.ToString("dddd");
            _valueMonth = _controlValue.ToString("MMM");
            _valueDate = _controlValue.ToString("MM-dd-yyyy");
            _valueTime = _controlValue.ToString("hh:mm");
        }

        /// <summary>
        /// Assign and initialize values to the component's properties;
        /// </summary>
        /// <param name="inputBox"></param>
        internal override void InitializePropertiesOfComponent()
        {
            InputControl=new xDateTime();
            Name = "ecDateSelector";
            InputControl.CustomFormat = "MM/dd/yyyy";
            InputControl.Format = DateTimePickerFormat.Long;
            InputControl.BorderStyle = ecBorderType.None;
            base.ControlToAdd = InputControl;
        }
        #endregion
    }
}
