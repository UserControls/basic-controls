﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ecControls
{
    // This class extends the standard DateTimePicker control for layouting
    // Used by the ecDateSelector User Control
    [ToolboxBitmap(typeof(DateTimePicker))]
    internal partial class xDateTime : DateTimePicker
    {
        private ecBorderType _borderStyle;
        private string _customFormat;
        private DateTimePickerFormat _format;
        private Color _foreColor;
        internal ecBorderType BorderStyle
        {
            get
            {
                return _borderStyle;
            }
            set
            {
                _borderStyle = value;
                RaisePaintEvent(this, null);
            }
        }

        new internal Color ForeColor
        {
            get
            {
                return _foreColor;
            }
            set
            {
                _foreColor = value;
                RaisePaintEvent(this, null);
            }
        }

        new internal string CustomFormat
        {
            get
            {
                return _customFormat;
            }
            set
            {
                _customFormat = value;
                base.CustomFormat = _customFormat;
            }
        }

        new internal DateTimePickerFormat Format
        {
            get
            {
                return _format;
            }
            set
            {
                _format = value;
                base.Format = _format;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            float fromTop;
            fromTop = (this.Height - Font.Height) / 2;

            if (_borderStyle != ecBorderType.None)
            {
                // Draw a bottom line of the border
                e.Graphics.DrawLine(Pens.Black, 0, this.ClientSize.Height - 1, this.ClientSize.Width, this.ClientSize.Height - 1);
                // Draw the rest of the border if not just underlining
                if (_borderStyle == ecBorderType.Single)
                {
                    // Draw a top line of the border
                    e.Graphics.DrawLine(Pens.Black, 0, 0, this.ClientSize.Width, 0);
                    // Draw the left line of the border
                    e.Graphics.DrawLine(Pens.Black, 0, 0, 0, this.ClientSize.Height - 1);
                    // Draw the right line of the border
                    e.Graphics.DrawLine(Pens.Black, this.ClientSize.Width - 1, 0, this.ClientSize.Width - 1, this.ClientSize.Height - 1);
                }
            }
            // Draw the text and set forecolor
            e.Graphics.DrawString(this.Text, this.Font, new SolidBrush(_foreColor), 0, fromTop);
            // Draw the calendar icon
            e.Graphics.DrawImage(Properties.Resources.Calendar, new Point(this.ClientRectangle.X + this.ClientRectangle.Width - (15 + ((this.Height - 15) / 2)), (this.Height - 15) / 2));
        }

        /// <summary>
        /// Set the default values
        /// </summary>
        private void SetDefaults()
        {
            _borderStyle = ecBorderType.None;
            _customFormat = "MM/dd/yyyy";
            _format = DateTimePickerFormat.Custom;
            _foreColor = Color.Black;
        }

        internal xDateTime() : base()
        {
            SetDefaults();
            SetStyle(ControlStyles.UserPaint, true);
        }
    }
}
