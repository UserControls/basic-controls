﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ecControls
{
    /// <summary>
    /// This is a user control utilizing the ListBox
    /// It implements the ecCombo Class and ecText's non-base components
    /// </summary>
    public partial class ecList : ecCombo
    {
        #region Input Component
        // Specifies ListBox to be added
        new internal ListBox InputControl;
        #endregion

        #region Variables
        private int _visibleItems;
        #endregion

        #region Exposed Properties
        /// <summary>
        /// Item Collection of the Control
        /// </summary>
        public new ListBox.ObjectCollection Items
        {
            get
            {
                return InputControl.Items;
            }
        }

        public int VisibleItems
        {
            get
            {
                return _visibleItems;
            }
            set
            {
                _visibleItems = value;
                RedrawControl(this, null);
            }
        } 
        #endregion

        #region Methods
        /// <summary>
        /// Set the ComboBox DataSource Properties
        /// </summary>
        internal override void SetDataSet()
        {
            InputControl.DataSource = new BindingSource(_dataSource, null);
            InputControl.DisplayMember = _displayMember;
            InputControl.ValueMember = _valueMember;
            InputControl.SelectedIndex = -1;
        }

        /// <summary>
        /// Method trigerred when control value is set
        /// </summary>
        internal override void ReturnValueSet()
        {
            InputControl.SelectedIndex = _controlValue;
            _controlIndex = _controlValue;

        }

        /// <summary>
        /// Clears the local variables that stores the controls value that correspond 
        /// to the specific input component of the control
        /// </summary>
        internal override void ClearComponents()
        {
            _controlValue = string.Empty;
            _controlIndex = -1;
        }

        /// <summary>
        /// Method to add or remove event handler specific for the input component of the control
        /// </summary>
        /// <param name="addRemove">Indicates whether to add or remove the event</param>
        internal override void EventHandlerForComponent(ecAction addRemove)
        {
            if (addRemove == ecAction.Add)
            {
                this.InputControl.SelectedValueChanged += new EventHandler(Value_Changed);
            }
            else
            {
                this.InputControl.SelectedValueChanged -= new EventHandler(Value_Changed);
            }
        }

        internal override void DrawComponent()
        {
            InputControl.Height = this.Font.Height * _visibleItems;
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Method to handle ComboBox Value change Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal override void Value_Changed(object sender, EventArgs e)
        {
            // If nothing has been selected, do not return anything
            if (InputControl.SelectedIndex == -1)
            {
                return;
            }

            _controlValue = InputControl.GetItemText(InputControl.SelectedItem);
            _controlIndex = InputControl.SelectedIndex;
            // Raise the event to the concrete class
            SelectionHasChanged?.Invoke(sender, e);
        }
        #endregion

        #region Initializers
        /// <summary>
        /// Assigns initial values to the local variables specific to the component
        /// </summary>
        internal override void InitializeSpecialVariables()
        {
            _visibleItems = 5;
        }

        /// <summary>
        /// Assign and initialize values to the component's properties;
        /// </summary>
        /// <param name="inputBox"></param>
        internal override void InitializePropertiesOfComponent()
        {
            InputControl=new ListBox();
            InputControl.BorderStyle = BorderStyle.None;
            this.Name = "ecList";
            base.ControlToAdd = InputControl;
        }
        #endregion

    }
}
