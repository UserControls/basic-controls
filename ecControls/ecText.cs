﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ecControls
{
    public partial class ecText : UserControl
    {
        #region version indicator
        // Hide this property from auto-complete
        [EditorBrowsable(EditorBrowsableState.Never)] 
        // Prevent auto-generation in designer
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] 
        public string WorkVersion
        {
            get
            {
                return "v 2.0";
            }
        }
        #endregion

        #region Input Component
        // Specifies TextBox to be added
        internal TextBox InputControl;
        internal Control ControlToAdd;
        #endregion

        #region Base Variables
        // Control Base Component Variables
        internal Panel PanelControl;
        internal Panel PanelLabel;
        internal Panel PanelInput;
        internal Label LabelField;
        internal Label LabelBorder;

        // Base Fields
        internal dynamic _controlValue;
        internal int _padding;
        internal ecStyle _style;
        internal int _labelPanelWidth;
        internal int _labelPanelHeight;
        internal Color _inputBackColor;
        internal Color _labelBackColor;
        internal string _labelText;
        internal BorderStyle _labelBorder;
        internal BorderStyle _fieldBorder;
        internal ContentAlignment _labelAlignment;
        internal Font _labelFont;
        internal Color _labelForeColor;
        internal Color _inputColor;
        #endregion

        #region Local Variables
        /*************************************************************************/
        /* 
         *     Local variables specific to the input component of the control.
         */
        /*************************************************************************/

        private HorizontalAlignment _inputAlignment; 
        private bool _mask;
        private CharacterCasing _valueCasing;
        private int _maxChars;
        /*************************************************************************/
        #endregion

        #region Base Exposed Properties
        /// <summary>
        /// Return Value of the Control
        /// </summary>
        [Browsable(false)]
        public dynamic ReturnValue
        {
            get
            {
                return _controlValue;
            }
            set
            {
                if(_controlValue == value)
                {
                    return;
                }
                _controlValue = value;
                ReturnValueSet();
            }
        }

        /// <summary>
        /// Style of the control
        /// </summary>
        public ecStyle Style
        {
            get
            {
                return _style;
            }
            set
            {
                _style = value;
                DrawControl();
            }
        }

        /// <summary>
        /// Distances between borders and controls
        /// </summary>
        public int BorderPadding
        {
            get
            {
                return _padding;
            }
            set
            {
                _padding = value;
                DrawControl();
            }
        }
        /// <summary>
        /// Border setting of the input area
        /// </summary>
        public BorderStyle InputBorderSetting
        {
            get
            {
                return _fieldBorder;
            }
            set
            {
                _fieldBorder = value;
                DrawControl();
            }
        }
        /// <summary>
        /// Background color ot the input area
        /// </summary>
        public Color InputBackColor
        {
            get
            {
                return _inputBackColor;
            }
            set
            {
                _inputBackColor = value;
                DrawControl();
            }
        }
        /// <summary>
        /// Text Color of the input compoment
        /// </summary>
        public new Color ForeColor
        {
            get
            {
                return _inputColor;
            }
            set
            {
                _inputColor = value;
                base.ForeColor = _inputColor;
                DrawControl();
            }
        }
        /// <summary>
        /// Font of the control's label
        /// </summary>
        public Font LabelFont
        {
            get
            {
                return _labelFont;
            }
            set
            {
                _labelFont = value;
                DrawControl();
            }
        }
        /// <summary>
        /// Text Color of the control's label
        /// </summary>
        public Color LabelForeColor
        {
            get
            {
                return _labelForeColor;
            }
            set
            {
                _labelForeColor = value;
                DrawControl();
            }
        }
        /// <summary>
        /// Border setting of the label area
        /// </summary>
        public BorderStyle LabelBorderSetting
        {
            get
            {
                return _labelBorder;
            }
            set
            {
                _labelBorder = value;
                DrawControl();
            }
        }
        /// <summary>
        /// Background color ot the label area
        /// </summary>
        public Color LabelBackColor
        {
            get
            {
                return _labelBackColor;
            }
            set
            {
                _labelBackColor = value;
                DrawControl();
            }
        }
        /// <summary>
        /// Control's label alignment
        /// </summary>
        public ContentAlignment LabelAlignment
        {
            get
            {
                return _labelAlignment;
            }
            set
            {
                _labelAlignment = value;
                DrawControl();
            }
        }
        /// <summary>
        /// Text of the control's label
        /// </summary>
        public string LabelText
        {
            get
            {
                return _labelText;
            }
            set
            {
                _labelText = value;
                DrawControl();
            }
        }

        /// <summary>
        /// Width of the label area
        /// Used only on LeftLabel style
        /// </summary>
        public int LabelWidth
        {
            get
            {
                return _labelPanelWidth;
            }
            set
            {
                _labelPanelWidth = value;
                DrawControl();
            }
        }

        /// <summary>
        /// Height of the label area.
        /// Used only on TopLabel style
        /// </summary>
        public int LabelHeight
        {
            get
            {
                return _labelPanelHeight;
            }
            set
            {
                _labelPanelHeight = value;
                DrawControl();
            }
        }

        #endregion

        #region Local Exposed Properties
        /*************************************************************************/
        /* 
         *     Exposed Properties specific to the input component of the control.
         */
        /*************************************************************************/

        /// <summary>
        /// Alignment of the TextBox
        /// </summary>
        public HorizontalAlignment InputAlignment
        {
            get
            {
                return _inputAlignment;
            }
            set
            {
                _inputAlignment = value;
                AddInputComponent(ControlToAdd);
                DrawControl();
            }
        }

        /// <summary>
        /// Indicates whether texted type is shown or masked
        /// </summary>
        public bool Mask
        {
            get
            {
                return _mask;
            }
            set
            {
                _mask = value;
                AddInputComponent(ControlToAdd); 
                DrawControl();
            }
        }

        /// <summary>
        /// Specifies the maximum characters that can be entered
        /// </summary>
        public int MaxChars
        {
            get
            {
                return _maxChars;
            }
            set
            {
                _maxChars = value;
                AddInputComponent(ControlToAdd);
                DrawControl();
            }
        }

        /// <summary>
        /// Specifies the casing of the text entered
        /// </summary>
        public CharacterCasing ValueCasing
        {
            get
            {
                return _valueCasing;
            }
            set
            {
                _valueCasing = value;
                AddInputComponent(ControlToAdd);
                DrawControl();
            }
        }
        /*************************************************************************/
        #endregion

        #region Base Exposed Events
        // Event raised before a key is pressed
        public new PreviewKeyDownEventHandler PreviewKeyDown;
        // Event raised when a key is pressed
        public new KeyEventHandler KeyDown;
        #endregion

        #region Local Exposed Event
        /*************************************************************************/
        /* 
         *     Exposed Event specific to the input component of the control.
         */
        /*************************************************************************/

        // Event raised when the content of the control changes
        public EventHandler TextHasChanged;
        /*************************************************************************/
        #endregion

        #region Base Methods
        /// <summary>
        /// Add the input component to the control
        /// </summary>
        internal void AddInputComponent(Control NewControl)
        {
            // Remove any existing instance of the component
            if (PanelInput.Controls.Contains(NewControl))
            {
                // Decouple the event handlers
                NewControl.Resize -= new System.EventHandler(RedrawControl);
                NewControl.PreviewKeyDown -= new PreviewKeyDownEventHandler(Preview_KeyDown);
                NewControl.KeyDown -= new KeyEventHandler(Key_Down);
                EventHandlerForComponent(ecAction.Remove);

                ClearComponents();
                PanelInput.Controls.Remove(NewControl);
            }

            // Create component and set its properties 

            //InitializePropertiesOfComponent();

            // Add the specific component to the control
            PanelInput.Controls.Add(NewControl);

            // Add Event Handlers
            NewControl.Resize += new System.EventHandler(RedrawControl);
            NewControl.PreviewKeyDown -= new PreviewKeyDownEventHandler(Preview_KeyDown);
            NewControl.KeyDown -= new KeyEventHandler(Key_Down);
            EventHandlerForComponent(ecAction.Add);
        }

        /// <summary>
        /// Draw the control by calling the Common Drawing Methods
        /// </summary>
        internal void DrawControl()
        {
            ControlPresenter.DrawContainers(this);
            ControlPresenter.DrawOnStyle(this);
        }
        #endregion

        #region Local Methods
        /***********************************************************************/
        /* 
         *        Methods specific to the input component of the control.
         *        They may be called from other Base functions.
         */
        /***********************************************************************/
        /// <summary>
        /// Clears the local variable that stores the controls value that correspond 
        /// to the specific input component of the control
        /// </summary>
        internal virtual void ClearComponents()
        {
            _controlValue = string.Empty;
        }

        /// <summary>
        /// Method trigerred when control value is set
        /// </summary>
        internal virtual void ReturnValueSet()
        {
            InputControl.Text=_controlValue;
        }

        /// <summary>
        /// Method to add or remove event handler specific for the input component of the control
        /// </summary>
        /// <param name="addRemove">Indicates whether to add or remove the event</param>
        internal virtual void EventHandlerForComponent(ecAction addRemove)
        {
            if (addRemove == ecAction.Add)
            {
                InputControl.TextChanged += new EventHandler(Value_Changed);
            }
            else
            {
                InputControl.TextChanged -= new EventHandler(Value_Changed);
            }
        }
        internal virtual void DrawComponent()
        {

        }
        /***********************************************************************/
        #endregion

        #region Base Event Handlers
        /// <summary>
        /// Method to handle Control Resize Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void RedrawControl(object sender, EventArgs e)
        {
            DrawComponent();
            DrawControl();
        }

        /// <summary>
        /// Method that invokes the concrete control's Preview KeyDown event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void Preview_KeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            PreviewKeyDown?.Invoke(sender, e);
        }

        /// <summary>
        /// Method that invokes the concrete control's KeyDown event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void Key_Down(object sender, KeyEventArgs e)
        {
            KeyDown?.Invoke(sender, e);
        }
        #endregion

        #region Local Event Handlers
        /*************************************************************************/
        /* 
         *     Event Handler specific to the input component of the control.
         */
        /*************************************************************************/

        /// <summary>
        /// Method to handle TextBox Value change Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal virtual void Value_Changed(object sender, EventArgs e)
        {
            _controlValue = InputControl.Text;
            TextHasChanged?.Invoke(sender, e); // Invokes the Exposed Event
        }
        /*************************************************************************/
        #endregion

        #region Base Initializers
        /// <summary>
        /// Assign initial values to fields
        /// </summary>
        internal void InitializeVariables()
        {
            _padding = 6;
            _labelPanelWidth = 100;
            _labelPanelHeight = 25;
            _inputBackColor=Color.LightGray;
            _labelBackColor=SystemColors.ControlDark;
            _labelAlignment = ContentAlignment.MiddleCenter;
            _labelBorder = BorderStyle.FixedSingle;
            _labelText = this.Name;
            _fieldBorder = BorderStyle.FixedSingle;
            _style = ecStyle.LeftLabel;
            _labelFont = this.Font;
            _inputColor = base.ForeColor;
            _labelForeColor = this.ForeColor;
        }
        #endregion

        #region Local Initializers
        /*************************************************************************/
        /* 
         *     Initializer Methods specific to the input component of the control.
         */
        /*************************************************************************/

        /// <summary>
        /// Assigns initial values to the local variables specific to the component
        /// </summary>
        internal virtual void InitializeSpecialVariables()
        {
            _inputAlignment = HorizontalAlignment.Left;
            _mask = false;
            _valueCasing = CharacterCasing.Normal;
            _padding = 10;
            _maxChars = 256;
        }

        /// <summary>
        /// Assign and initialize values to the component's properties;
        /// </summary>
        /// <param name="inputBox"></param>
        internal virtual void InitializePropertiesOfComponent()
        {
            InputControl = new TextBox();
            InputControl.BorderStyle = BorderStyle.None;
            InputControl.TextAlign = HorizontalAlignment.Left;
            InputControl.UseSystemPasswordChar = false;
            InputControl.CharacterCasing = CharacterCasing.Normal;
            InputControl.MaxLength = 256;
            this.Name = "ecText";
            ControlToAdd = InputControl;
        }
        /*************************************************************************/
        #endregion

        #region Constructor
        /// <summary>
        /// Create an instance of the Control
        /// </summary>
        public ecText()
        {
            InitializeComponent();
            InitializePropertiesOfComponent();
            InitializeVariables();
            InitializeSpecialVariables();
            ControlPresenter.AddBasicComponents(this);
            AddInputComponent(ControlToAdd);
            ClearComponents();
            DrawControl();
            // Define Event handler to handle Control Resizes
            this.Resize += new EventHandler(RedrawControl);
        }
        #endregion
    }
}
