﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecControls
{
    public enum ecStyle
    {
        LeftLabel,
        TopLabel,
        BorderLabel
    }
    public enum ecKeyValueSelection
    {
        Key,
        Value
    }
    public enum ecAction
    {
        Add,
        Remove
    }

    public enum ecBorderType
    {
        None,
        Single,
        Underline
    }
}
