﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ecControls
{
    /// <summary>
    /// The base class that handles the presentation of all ecControls
    /// </summary>
    internal static class ControlPresenter
    {

        /// <summary>
        /// Creates the basic components of the control
        /// </summary>
        /// <param name="ecControl"></param>
        internal static void AddBasicComponents(dynamic ecControl)
        {
            ecControl.PanelControl = new Panel();
            ecControl.PanelLabel = new Panel();
            ecControl.LabelField = new Label();
            ecControl.PanelInput = new Panel();
            ecControl.LabelBorder = new Label();

            ecControl.PanelControl.SuspendLayout();
            ecControl.PanelLabel.SuspendLayout();

            // PanelControl
            ecControl.PanelControl.BorderStyle = BorderStyle.FixedSingle;
            ecControl.PanelControl.Location = new Point(0, 5);
            ecControl.PanelControl.Name = "PanelControl";
            ecControl.PanelControl.Size = new Size(282, 51);
            ecControl.PanelControl.TabIndex = 0;
            ecControl.PanelControl.Controls.Add(ecControl.PanelLabel);
            ecControl.PanelControl.Controls.Add(ecControl.PanelInput);

            // PanelLabel
            ecControl.PanelLabel.BorderStyle = BorderStyle.FixedSingle;
            ecControl.PanelLabel.Location = new Point(3, 8);
            ecControl.PanelLabel.Name = "PanelLabel";
            ecControl.PanelLabel.Size = new Size(91, 31);
            ecControl.PanelLabel.TabIndex = 2;
            ecControl.PanelLabel.Controls.Add(ecControl.LabelField);

            // LabelField
            ecControl.LabelField.Dock = DockStyle.Fill;
            ecControl.LabelField.Location = new Point(0, 0);
            ecControl.LabelField.Name = "LabelField";
            ecControl.LabelField.Size = new Size(89, 29);
            ecControl.LabelField.TabIndex = 0;
            ecControl.LabelField.Text = "label2";
            ecControl.LabelField.TextAlign = ContentAlignment.MiddleCenter;

            // PanelInput
            ecControl.PanelInput.BorderStyle = BorderStyle.FixedSingle;
            ecControl.PanelInput.Location = new Point(100, 8);
            ecControl.PanelInput.Name = "PanelInput";
            ecControl.PanelInput.Size = new Size(177, 31);
            ecControl.PanelInput.TabIndex = 1;

            // LabelBorder
            ecControl.LabelBorder.AutoSize = true;
            ecControl.LabelBorder.Location = new Point(5, 0 - (ecControl.BorderPadding / 2));
            ecControl.LabelBorder.Name = "LabelBorder";
            ecControl.LabelBorder.TextAlign = ContentAlignment.BottomCenter;
            ecControl.LabelBorder.TabIndex = 1;
            ecControl.LabelBorder.BackColor = Color.Transparent;
            ecControl.LabelBorder.Text = "label1";

            ecControl.Controls.Add(ecControl.LabelBorder);
            ecControl.Controls.Add(ecControl.PanelControl);

            ecControl.PanelControl.ResumeLayout(false);
            ecControl.PanelLabel.ResumeLayout(false);
        }

        /// <summary>
        /// Draws the basic components of the control
        /// </summary>
        /// <param name="ecControl"></param>
        internal static void DrawContainers(dynamic ecControl)
        {
            ecControl.PanelLabel.BorderStyle = ecControl.LabelBorderSetting;
            ecControl.PanelInput.BorderStyle = ecControl.InputBorderSetting;

            ecControl.PanelInput.BackColor = ecControl.InputBackColor;
            ecControl.InputControl.BackColor = ecControl.InputBackColor;
            ecControl.InputControl.ForeColor = ecControl.ForeColor;
            ecControl.LabelField.BackColor = ecControl.LabelBackColor;

            ecControl.LabelField.TextAlign = ecControl.LabelAlignment;
            ecControl.LabelField.Font = ecControl.LabelFont;
            ecControl.LabelField.Text = ecControl.LabelText;
            ecControl.LabelField.ForeColor = ecControl.LabelForeColor;

            ecControl.LabelBorder.Font = ecControl.LabelFont;
            ecControl.LabelBorder.Text = ecControl.LabelText;
            ecControl.LabelBorder.ForeColor = ecControl.LabelForeColor;
        }

        /// <summary>
        /// Styles the control as per the specified style type
        /// </summary>
        /// <param name="ecControl"></param>
        internal static void DrawOnStyle(dynamic ecControl)
        {
            ecControl.PanelInput.Height = ecControl.BorderPadding + ecControl.InputControl.Height + ecControl.BorderPadding;

            ecControl.PanelInput.Visible = true;

            if (ecControl.Style == ecStyle.LeftLabel)
            {
                // Hide the control's border component as the control only shows the 
                // Label Panel and Input Panels
                ecControl.LabelBorder.Visible = false;
                ecControl.PanelControl.BorderStyle = BorderStyle.None;
                ecControl.PanelControl.Location = new Point(0, 0);

                ecControl.PanelLabel.Visible = true;
                ecControl.PanelLabel.Location = new Point(0, 0);
                ecControl.PanelLabel.Height = ecControl.PanelInput.Height;

                // Label Width is set to the LabelWidth property
                ecControl.PanelLabel.Width = ecControl.LabelWidth;

                // Adjust a point when border is showing
                if (ecControl.LabelBorderSetting == BorderStyle.None)
                {
                    ecControl.PanelInput.Location = new Point(ecControl.PanelLabel.Width, 0);
                }
                else
                {
                    ecControl.PanelInput.Location = new Point(ecControl.PanelLabel.Width - 1, 0);
                }

                // Adjust the size of the panels
                ecControl.Height = ecControl.PanelInput.Height;
                ecControl.PanelInput.Width = ecControl.Width - ecControl.PanelLabel.Width;
                ecControl.InputControl.Width = ecControl.PanelInput.Width - ecControl.BorderPadding - ecControl.BorderPadding;
                ecControl.PanelControl.Width = ecControl.PanelLabel.Width + ecControl.PanelInput.Width;
                ecControl.PanelControl.Height = ecControl.PanelInput.Height;

                // Position the input component to the center of the input panel
                ecControl.InputControl.Left = (ecControl.PanelInput.Width - ecControl.InputControl.Width) / 2;
                ecControl.InputControl.Top = (ecControl.PanelInput.Height - ecControl.InputControl.Height) / 2;
            }
            else if (ecControl.Style == ecStyle.TopLabel)
            {
                // Hide the control's border component as the control only shows the 
                // Label Panel and Input Panels
                ecControl.LabelBorder.Visible = false;
                ecControl.PanelControl.BorderStyle = BorderStyle.None;
                ecControl.PanelControl.Location = new Point(0, 0);

                ecControl.PanelLabel.Visible = true;
                ecControl.PanelLabel.Location = new Point(0, 0);
                ecControl.PanelLabel.Height = ecControl.LabelHeight;

                // Label Width is set to the control's Width
                ecControl.PanelLabel.Width = ecControl.Width;

                // Adjust a point when border is showing
                if (ecControl.InputBorderSetting == BorderStyle.None || ecControl.LabelBorderSetting == BorderStyle.None)
                {
                    ecControl.PanelInput.Location = new Point(0, ecControl.PanelLabel.Height);
                }
                else
                {
                    ecControl.PanelInput.Location = new Point(0, ecControl.PanelLabel.Height - 1);
                }

                // Adjust the size of the panels
                ecControl.Height = ecControl.PanelLabel.Height + ecControl.PanelInput.Height;
                ecControl.PanelInput.Width = ecControl.Width;
                ecControl.PanelControl.Width = ecControl.PanelInput.Width;
                ecControl.InputControl.Width = ecControl.PanelInput.Width - ecControl.BorderPadding - ecControl.BorderPadding;
                ecControl.PanelControl.Height = ecControl.PanelLabel.Height + ecControl.PanelInput.Height;

                // Position the input component to the center of the input panel
                ecControl.InputControl.Left = (ecControl.PanelInput.Width - ecControl.InputControl.Width) / 2;
                ecControl.InputControl.Top = (ecControl.PanelInput.Height - ecControl.InputControl.Height) / 2;
            }
            else if(ecControl.Style == ecStyle.BorderLabel)
            {
                // Show the control's border component and its attached Label 
                ecControl.LabelBorder.Visible = true;
                ecControl.PanelControl.BorderStyle = BorderStyle.FixedSingle;
                // Position the border a little bit (pad) below the top to appear in the middle of the attached
                // label height
                ecControl.PanelControl.Location = new Point(0, ecControl.BorderPadding);

                // Hide the Field indicator panel as what will be shown is the attached label
                ecControl.PanelLabel.Visible = false;
                // Remove the border of the input panel as what will be shown is the control's border
                ecControl.PanelInput.BorderStyle = BorderStyle.None;
                // PanelInput is moved a bit down (quarter of padding) to prevent overlap with Label
                ecControl.PanelInput.Location = new Point(0, ecControl.BorderPadding / 4);

                // Adjust the size of the panels
                ecControl.PanelControl.Width = ecControl.Width;
                ecControl.PanelInput.Width = ecControl.PanelControl.Width;
                ecControl.InputControl.Width = ecControl.PanelInput.Width - ecControl.BorderPadding - ecControl.BorderPadding;
                // Add 2x the overlap allowance (Padding/2) to the PanelControl Height
                ecControl.PanelControl.Height = ecControl.PanelInput.Height + (ecControl.BorderPadding / 2);
                ecControl.Height = ecControl.PanelControl.Height + ecControl.BorderPadding;

                // Position the input component to the center of the control
                ecControl.InputControl.Left = (ecControl.PanelControl.Width - ecControl.InputControl.Width) / 2;
                ecControl.InputControl.Top = (ecControl.PanelControl.Height - ecControl.InputControl.Height) / 2;
            }
        }
    }
}
