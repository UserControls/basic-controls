﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ecControls
{
    /// <summary>
    /// This is a user control utilizing the ComboBox
    /// It implements the ecText Class and overrides the non-base components
    /// </summary>
    public partial class ecCombo : ecText
    {
        #region Input Component
        // Specifies ComboBox to be added
        new internal ComboBox InputControl;
        #endregion

        #region Variables
        internal object _dataSource;
        internal string _displayMember;
        internal string _valueMember;
        internal int _controlIndex;
        internal ecKeyValueSelection _dictionaryReturnType;
        #endregion

        #region Exposed Properties

        /// <summary>
        /// Item Collection of the Control
        /// </summary>
        public ComboBox.ObjectCollection Items
        {
            get
            {
                return this.InputControl.Items; ;
            }
        }
        /// <summary>
        /// Source of Data for the Combobox
        /// This gets overwritten by the DataSet when DataSet property is used.
        /// </summary>
        [Browsable(false)]
        public object DataSource
        {
            get
            {
                return _dataSource;
            }
            set
            {
                _dataSource = value;
                if (_dataSource != null)
                {
                    SetDataSet();
                }
            }
        }

        /// <summary>
        /// The column of the DataSource displayed in the combobox
        /// This gets overwritten by the DataSet when DataSet property is used.
        /// </summary>
        [Browsable(false)]
        public string DisplayMember
        {
            get
            {
                return _displayMember;
            }
            set
            {
                _displayMember = value;
            }
        }

        /// <summary>
        /// The column of the DataSource returned by combobox when an item is selected
        /// This gets overwritten by the DataSet when DataSet property is used. 
        /// </summary>
        [Browsable(false)]
        public string ValueMember
        {
            get
            {
                return _valueMember;
            }
            set
            {
                _valueMember = value;
            }
        }

        /// <summary>
        /// Sets a dictionary to use as DataSource
        /// Displays the dictionary's value 
        /// Uses the dictionary key or value as return type as specifed by the
        /// property DictionaryReturnType
        /// </summary>
        [Browsable(false)]
        public Dictionary<int, string> DataSet
        {
            set
            {
                if (value == null)
                {
                    return;
                }
                _displayMember = "Value";
                if (_dictionaryReturnType == ecKeyValueSelection.Key)
                {
                    _valueMember = "Key";
                }
                else
                {
                    _valueMember= "Value";
                }
                _dataSource = value;

                SetDataSet();
            }
        }

        /// <summary>
        /// Specifies the return type (key or value) when using the DataSet property
        /// </summary>
        [Browsable(false)]
        public ecKeyValueSelection DictionaryReturnType
        {
            get
            {
                return _dictionaryReturnType;
            }
            set
            {
                _dictionaryReturnType = value;
            }

        }

        /// <summary>
        /// Uses index as Return Value of the Control
        /// </summary>
        [Browsable(false)]
        public int SelectedIndex
        {
            get
            {
                return _controlIndex;
            }
            set
            {
                if(_controlIndex == value)
                {
                    return;
                }
                _controlIndex = value;
                ReturnValueSet();
            }
        }
        #endregion

        #region Hide Unused Base Properties and Events
        [Browsable(false)] // Hides this property in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new HorizontalAlignment InputAlignment
        {
            get
            {
                return HorizontalAlignment.Left;
            }
        }

        [Browsable(false)] // Hides this property in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new bool Mask
        {
            get
            {
                return false;
            }
        }

        [Browsable(false)] // Hides this property in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new int MaxChars
        {
            get
            {
                return 0;
            }
        }

        [Browsable(false)] // Hides this property in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new CharacterCasing ValueCasing
        {
            get
            {
                return CharacterCasing.Normal ;
            }
        }

        [Browsable(false)] // Hides this Event in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this Event from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new EventHandler TextHasChanged;
        #endregion

        #region Exposed Event
        // Event raised when the content of the control changes
        public EventHandler SelectionHasChanged;
        #endregion

        #region Methods
        /// <summary>
        /// Set the ComboBox DataSource Properties
        /// </summary>
        internal virtual void SetDataSet()
        {
            InputControl.DataSource = new BindingSource(_dataSource, null);
            InputControl.DisplayMember = _displayMember;
            InputControl.ValueMember = _valueMember;
            InputControl.SelectedIndex = -1;
        }

        /// <summary>
        /// Method trigerred when control value is set
        /// </summary>
        internal override void ReturnValueSet()
        {
            InputControl.SelectedIndex = _controlIndex;

            // If control is not using DataSource return the position of the selection, 
            if (_dataSource != null)
            {
                _controlValue = InputControl.SelectedValue.ToString();
            }
            else
            {
                _controlValue = InputControl.SelectedItem.ToString();
            }
        }

        /// <summary>
        /// Clears the local variables that stores the controls value that correspond 
        /// to the specific input component of the control
        /// </summary>
        internal override void ClearComponents()
        {
            _controlValue = string.Empty;
            _controlIndex = -1;
        }

        /// <summary>
        /// Method to add or remove event handler specific for the input component of the control
        /// </summary>
        /// <param name="addRemove">Indicates whether to add or remove the event</param>
        internal override void EventHandlerForComponent(ecAction addRemove)
        {
            if (addRemove == ecAction.Add)
            {
                this.InputControl.SelectedValueChanged += new EventHandler(Value_Changed);
            }
            else
            {
                this.InputControl.SelectedValueChanged -= new EventHandler(Value_Changed);
            }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Method to handle ComboBox Value change Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal override void Value_Changed(object sender, EventArgs e)
        {
            // If nothing has been selected, do not return anything
            if (InputControl.SelectedIndex == -1)
            {
                return;
            }

            // If control is not using DataSource return the position of the selection, 
            if (_dataSource != null)
            {
                _controlValue = InputControl.SelectedValue.ToString();
            }
            else
            {
                _controlValue = InputControl.SelectedItem.ToString();
            }
            _controlIndex = InputControl.SelectedIndex;
            // Raise the event to the concrete class
            SelectionHasChanged?.Invoke(sender, e);
        }

        #endregion

        #region Initializers
        /// <summary>
        /// Assigns initial values to the local variables specific to the component
        /// </summary>
        internal override void InitializeSpecialVariables()
        {
            _dictionaryReturnType = ecKeyValueSelection.Key;
        }

        /// <summary>
        /// Assign and initialize values to the component's properties;
        /// </summary>
        /// <param name="inputBox"></param>
        internal override void InitializePropertiesOfComponent()
        {
            this.InputControl=new ComboBox();
            this.InputControl.FlatStyle = FlatStyle.Flat;
            this.InputControl.DropDownStyle = ComboBoxStyle.DropDownList;
            this.Name = "ecCombo";
            base.ControlToAdd = InputControl;
        }
        #endregion
    }
}
