﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using xControls;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace ecControls
{
    /// <summary>
    /// This is a user control utilizing the ecRadioList custom control
    /// It implements the ecCombo Class and ecText's non-base components
    /// </summary>
    public partial class ecRadioSelection : ecCombo
    {
        #region Input Component
        // Specifies RadioList to be added
        new internal ecRadioList InputControl;
        #endregion

        #region Variables
        private ecOrientation _inputAlignment;
        #endregion

        #region Exposed Properties
        /// <summary>
        /// Alignment of the TextBox
        /// </summary>
        public new ecOrientation InputAlignment
        {
            get
            {
                return _inputAlignment;
            }
            set
            {
                _inputAlignment = value;
                InputControl.Orientation = _inputAlignment;
                AddInputComponent(ControlToAdd);
                DrawControl();
            }
        }

        /// <summary>
        /// Return Index Value of the Control
        /// </summary>
        [Browsable(false)]
        public dynamic ReturnIndex
        {
            get
            {
                return _controlIndex;
            }
            set
            {
                if (_controlIndex == value)
                {
                    return;
                }
                _controlIndex = value;
                ReturnValueSet();
            }
        }

        public new ObservableCollection<string> Items
        {
            get
            {
                return InputControl.Items; 
            }
        }
        #endregion

        #region Hide Unused Base Properties

        [Browsable(false)] // Hides this property in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new object DataSource
        {
            get
            {
                return null;
            }
        }

        [Browsable(false)] // Hides this property in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new string DisplayMember
        {
            get
            {
                return string.Empty;
            }
        }

        [Browsable(false)] // Hides this property in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new string ValueMember
        {
            get
            {
                return string.Empty;
            }
         }

        [Browsable(false)] // Hides this property in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new object DataSet
        {
            get
            {
                return null;
            }
        }

        [Browsable(false)] // Hides this property in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new object DictionaryReturnType
        {
            get
            {
                return null;
            }
        }

        [Browsable(false)] // Hides this property in the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hides this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevents auto-generation in designer
        public new int SelectedIndex
        {
            get
            {
                return 0;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Clears the local variable that stores the controls value that correspond 
        /// to the specific input component of the control
        /// </summary>
        internal override void ClearComponents()
        {
            _controlIndex = InputControl.ReturnIndex;
            _controlValue = InputControl.ReturnText;
        }

        public void Clear()
        {
            Items.Clear();
        }

        /// <summary>
        /// Method to add or remove event handler specific for the input component of the control
        /// </summary>
        /// <param name="addRemove">Indicates whether to add or remove the event</param>
        internal override void EventHandlerForComponent(ecAction addRemove)
        {
            if (addRemove == ecAction.Add)
            {
                InputControl.RadioSelected += new EventHandler(Value_Changed);
                Items.CollectionChanged += new NotifyCollectionChangedEventHandler(Collection_Changed);
            }
            else
            {
                InputControl.RadioSelected -= new EventHandler(Value_Changed);
                Items.CollectionChanged -= new NotifyCollectionChangedEventHandler(Collection_Changed);
            }
        }

        private void Collection_Changed(object sender, NotifyCollectionChangedEventArgs e)
        {
            //ClearComponents();
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Method to handle Radio Selection Value change Events
        /// Sets Control's Value and Control's Selected Index
        /// And invokes the exposed event (of the Base Class)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal override void Value_Changed(object sender, EventArgs e)
        {
            _controlValue = InputControl.ReturnText;
            _controlIndex = InputControl.ReturnIndex;
            SelectionHasChanged?.Invoke(sender, e); 
        }
        #endregion

        #region Initializers
        /// <summary>
        /// Assigns initial values to the local variables specific to the component
        /// </summary>
        internal override void InitializeSpecialVariables()
        {
            _inputAlignment = ecOrientation.Vertical;
        }

        /// <summary>
        /// Assign and initialize values to the component's properties;
        /// </summary>
        /// <param name="inputBox"></param>
        internal override void InitializePropertiesOfComponent()
        {
            InputControl = new ecRadioList();
            InputControl.BorderStyle = BorderStyle.None;
            InputControl.Orientation = _inputAlignment;
            this.Name = "ecRadioList";
            base.ControlToAdd = InputControl;
        }
        #endregion
    }
}
