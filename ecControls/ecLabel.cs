﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ecControls
{
    /// <summary>
    /// This is a user control utilizing the Label
    /// It implements the ecText Class and overrides the non-base components
    /// </summary>
    public partial class ecLabel : ecText
    {
        #region Input Component
        // Specifies Label to be added
        new internal Label InputControl;
        #endregion

        #region Variables
        private ContentAlignment _inputAlignment;
        private CharacterCasing _valueCasing;
        private string _designValue=string.Empty;
        #endregion

        #region Exposed Properties
        /// <summary>
        /// Alignment of the Presentor Label
        /// </summary>
        public new ContentAlignment InputAlignment
        {
            get
            {
                return _inputAlignment;
            }
            set
            {
                _inputAlignment = value;
                AddInputComponent(ControlToAdd);
                DrawControl();
            }
        }

        /// <summary>
        /// Specifies the casing of the text entered
        /// </summary>
        public new CharacterCasing ValueCasing
        {
            get
            {
                return _valueCasing;
            }
            set
            {
                _valueCasing = value;
                AddInputComponent(ControlToAdd);
                DrawControl();
            }
        }

        /// <summary>
        /// Design Value of the Control
        /// Used in designing forms
        /// The value set for this property at design time gets cleared at runtime
        /// and merges with the property ReturnValue.
        /// This property is not viewable in the editor.
        /// </summary>
        [DesignOnly(true)] 
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string DesignValue
        {
            get
            {
                return _controlValue;
            }
            set
            {
                _controlValue = value;
                InputControl.Text= _controlValue;
            }
        }
        #endregion

        #region Hide Unused Base Properties
        [Browsable(false)] // Do not show this property on the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hide this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevent auto-generation in designer
        public new bool Mask
        {
            get
            {
                return false;
            }
        }

        [Browsable(false)] // Do not show this property on the property grid
        [EditorBrowsable(EditorBrowsableState.Never)] // Hide this property from auto-complete
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // Prevent auto-generation in designer
        public new int MaxChars
        {
            get
            {
                return 0;
            }
        }
        #endregion

        #region Exposed Event
        // Event raised when the content of the control changes
        public new EventHandler TextHasChanged;
        #endregion

        #region Methods
        /// <summary>
        /// Clears the local variable that stores the controls value that correspond 
        /// to the specific input component of the control
        /// </summary>
        internal override void ClearComponents()
        {
            _controlValue = string.Empty;
        }

        /// <summary>
        /// Method trigerred when control value is set
        /// </summary>
        internal override void ReturnValueSet()
        {
            InputControl.Text = _controlValue;
            if (_valueCasing == CharacterCasing.Upper)
            {
                InputControl.Text.ToUpper();
            }
            else if (_valueCasing == CharacterCasing.Lower)
            {
                InputControl.Text.ToLower();
            }
        }

        /// <summary>
        /// Method to add or remove event handler specific for the input component of the control
        /// </summary>
        /// <param name="addRemove">Indicates whether to add or remove the event</param>
        internal override void EventHandlerForComponent(ecAction addRemove)
        {
            if (addRemove == ecAction.Add)
            {
                InputControl.TextChanged += new EventHandler(Value_Changed);
            }
            else
            {
                InputControl.TextChanged -= new EventHandler(Value_Changed);
            }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Method to handle Label Value change Events. 
        /// Raises TextHasChanged when the Label's text value changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal override void Value_Changed(object sender, EventArgs e)
        {
            TextHasChanged?.Invoke(sender, e); 
        }
        #endregion

        #region Initializers
        /// <summary>
        /// Assigns initial values to the local variables specific to the component
        /// </summary>
        internal override void InitializeSpecialVariables()
        {
            _inputAlignment = ContentAlignment.MiddleLeft;
            _valueCasing = CharacterCasing.Normal;
        }

        /// <summary>
        /// Assign and initialize values to the component's properties;
        /// </summary>
        /// <param name="inputBox"></param>
        internal override void InitializePropertiesOfComponent()
        {
            InputControl = new Label();
            InputControl.BorderStyle = BorderStyle.None;
            InputControl.TextAlign = ContentAlignment.MiddleLeft;
            this.Name = "ecLabel";
            base.ControlToAdd = InputControl;
        }
        #endregion
    }
}
